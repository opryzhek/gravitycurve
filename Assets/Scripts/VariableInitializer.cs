﻿using ScriptableObjectArchitecture;
using UnityEngine;

public class VariableInitializer : MonoBehaviour
{
    [SerializeField]
    private BaseVariable[] variables;
    [SerializeField]
    private BaseCollection[] collections;

    [SerializeField] private BoolVariable isTargeting;

    private void Awake()
    {
        foreach (var variable in variables)
        {
            variable.Reset();
        }

        foreach (var collection in collections)
        {
            collection.Clear();
        }

        isTargeting.Value = true;
    }
}
