﻿using System;
using System.Collections;
using System.Collections.Generic;
using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Image))]
public class RotateToTap : MonoBehaviour
{
    [SerializeField] private FloatVariable rotation;
    [SerializeField] private float rotationChangeSpeed;
    private RectTransform rectTransform;
    private Image image;

    private Vector2 rotationCenter;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        image.alphaHitTestMinimumThreshold = Mathf.Epsilon;
        rectTransform = GetComponent<RectTransform>();
        rotationCenter = new Vector2(Screen.width, 0);
    }

    private Vector2 prevPosition = Vector3.zero;

    public void OnDragStart()
    {
        prevPosition = Input.mousePosition;
    }

    public void OnDrag()
    {
        Vector2 currentPosition = Input.mousePosition;
        var angle = Vector2.SignedAngle(prevPosition - rotationCenter, currentPosition - rotationCenter);
        rectTransform.rotation *= Quaternion.Euler(0, 0, angle);
        rotation.Value += angle * rotationChangeSpeed;
        prevPosition = currentPosition;
    }
}
