using System;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace UI
{
    
    [RequireComponent(typeof(RectTransform))]
    public class Joystick: MonoBehaviour
    {
        [SerializeField] private GameEvent onDragStart;
        [SerializeField] private GameEvent onDrag;
        [SerializeField] private GameEvent onDragEnd;
        [SerializeField] private RectTransform stickRect;
        [SerializeField] private BoolReference isEnabled;
        [SerializeField] private BoolReference invertIsEnabled;
        [SerializeField] private Vector3Variable direction;
        private RectTransform rectTransform;
        private CanvasGroup canvasGroup;
        private float radius;
        private Vector3 startPosition;

        private void Start()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            rectTransform = GetComponent<RectTransform>();
            radius = rectTransform.rect.width / 2;
            
            onDragStart.AddListener(Show);
            onDragEnd.AddListener(Hide);
            onDrag.AddListener(ProcessStick);
            isEnabled.AddListener(() =>
            {
                if (!IsEnabled())
                {
                    Hide();
                }
            });
            Hide();
        }
        
        private void Show()
        {
            if (!IsEnabled())
            {
                return;
            }
            canvasGroup.alpha = 1;
            canvasGroup.blocksRaycasts = true;

            startPosition = Input.mousePosition;
            rectTransform.position = startPosition;
            stickRect.position = startPosition;
        }

        private void Hide()
        {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
            direction.Value = Vector3.zero;
        }

        private void ProcessStick()
        {
            if (!IsEnabled())
            {
                return;
            }
            
            var diff = Input.mousePosition - startPosition;
            if (diff.sqrMagnitude > radius*radius)
            {
                diff.Normalize();
                diff *= radius;
            }

            direction.Value = diff / radius;
            stickRect.position = startPosition + diff;
        }

        private void Update()
        {
            if (Input.touchCount == 2)
            {
                Hide();
            }
        }

        private bool IsEnabled()
        {
            return (invertIsEnabled ^ isEnabled) && Input.touchCount != 2; // ignore zoom gesture
        }
    }
}