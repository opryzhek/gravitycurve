using System.Collections;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace UI
{
    public class FloatChanger: MonoBehaviour
    {
        public FloatVariable variable;
        public FloatReference amountPerSecond;
        public FloatReference amountMultiplier;
        public AnimationCurve curve;
        public FloatReference curveDuration;

        private IEnumerator valueChanger;

        public void StartChanging()
        {
            if(valueChanger!=null) StopCoroutine(valueChanger);
            valueChanger = ChangeValue();
            StartCoroutine(valueChanger);
        }

        public void StopChanging()
        {
            StopCoroutine(valueChanger);
            valueChanger = null;
        }

        private IEnumerator ChangeValue()
        {
            var changingTime = 0f;
            while (changingTime < curveDuration)
            {
                changingTime += Time.deltaTime;
                variable.Value += curve.Evaluate(changingTime / curveDuration) 
                                  * amountPerSecond * amountMultiplier * Time.deltaTime;
                yield return 0;
            }

            while (true)
            {
                variable.Value += amountPerSecond * amountMultiplier * Time.deltaTime;
                yield return 0;
            }
        }
    }
}