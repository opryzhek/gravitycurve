using System;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class VisibilityToggle : MonoBehaviour
    {
        [SerializeField] private BoolReference visible;
        [SerializeField] private BoolReference invert;
        
        private CanvasGroup canvasGroup;

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            visible.AddListener(Invalidate);
            invert.AddListener(Invalidate);
            Invalidate();
        }

        private void Invalidate()
        {
            if (visible ^ invert)
            {
                Show();
            }
            else
            {
                Hide();
            }
        }

        private void Show()
        {
            canvasGroup.alpha = 1;
            canvasGroup.blocksRaycasts = true;
        }

        private void Hide()
        {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
        }
    }
}