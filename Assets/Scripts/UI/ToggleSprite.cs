﻿using System;
using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Image))]
    [RequireComponent(typeof(Button))]
    public class ToggleSprite : MonoBehaviour
    {
        [SerializeField] private BoolVariable isEnabled;
        [SerializeField] private Sprite enabledSprite;
        [SerializeField] private Sprite disabledSprite;
        public bool IsEnabled
        {
            get => isEnabled;
            set => isEnabled.Value = value;
        }

        private Image image;
        private Button button;
        
        void Start()
        {
            button = GetComponent<Button>();
            image = GetComponent<Image>();

            button.image = image;
            button.onClick.AddListener(Toggle);
            IsEnabled = isEnabled;
            isEnabled.AddListener(UpdateImage);
            UpdateImage();
        }

        public void Toggle()
        {
            IsEnabled = !IsEnabled;
        }

        private void UpdateImage()
        {
            image.sprite = isEnabled.Value ? enabledSprite : disabledSprite;
        }
        
    }
}
