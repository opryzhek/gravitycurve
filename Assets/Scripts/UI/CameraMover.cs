﻿using System;
using System.Collections;
using System.Collections.Generic;
using ScriptableObjectArchitecture;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraMover : MonoBehaviour
{
    
    [SerializeField] private GameEvent onDragStart;
    [SerializeField] private GameEvent onDrag;
    [SerializeField] private BoolVariable isMovingModeDisabled;
    [SerializeField] private FloatReference maxMovingSpeed;
    public float minHeight;
    public float maxHeight;
    public float mouseScrollSpeed = 1;
    public float touchScrollSpeed = 0.1f;
    public Transform target; 
    private Vector3 dragStartPosition;
    private Camera mCamera;
    private Vector3 initialPosition;
    private Vector3 offset = Vector3.zero;

    private bool isZooming;
    private void Start()
    {
        initialPosition = transform.position;
        mCamera = GetComponent<Camera>();
        onDragStart.AddListener(OnDragStart);
        onDrag.AddListener(() =>
        {
            if (isMovingModeDisabled.Value) return;
            if (isZooming) return;
            var direction = dragStartPosition - MouseWorldPosition();
            transform.position += direction;
        });
        
    }

    private void OnDragStart()
    {
        if (isMovingModeDisabled.Value) return;
        dragStartPosition = MouseWorldPosition();
    }

    private Vector3 MouseWorldPosition()
    {
        var pos = Input.mousePosition;
        if (Input.touchCount == 2)
        {
            
            var touchZero = Input.GetTouch(0);
            var touchOne = Input.GetTouch(1);
            if (touchZero.phase != TouchPhase.Ended && touchOne.phase != TouchPhase.Ended)
            {
                pos = (Input.GetTouch(0).position + Input.GetTouch(1).position) / 2;
            }
            else if(touchOne.phase != TouchPhase.Ended)
            {
                pos = touchOne.position;
            }
            else if(touchZero.phase != TouchPhase.Ended)
            {
                pos = touchZero.position;
            }
        }
        return ScreenToWorldPosition(pos);
    }

    private Vector3 ScreenToWorldPosition(Vector3 position)
    {
        var pos = position;
        pos.z = -transform.position.z;
        pos = mCamera.ScreenToWorldPoint(pos);
        return pos;
    }

    private void Update()
    {
        if(Input.touchCount == 2)
        {
            isZooming = true;
            var touchZero = Input.GetTouch(0);
            var touchOne = Input.GetTouch(1);

            var touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            var touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            var prevMagnitude = ScreenToWorldPosition(touchZeroPrevPos - touchOnePrevPos).magnitude;
            var currentMagnitude = ScreenToWorldPosition(touchZero.position - touchOne.position).magnitude;

            var difference = currentMagnitude - prevMagnitude;

            Zoom(difference * touchScrollSpeed);
            if (touchZero.phase == TouchPhase.Ended || touchOne.phase == TouchPhase.Ended)
            {
                isZooming = false;
                OnDragStart();
            }
        }
        Zoom(Input.GetAxis("Mouse ScrollWheel") * mouseScrollSpeed);
        if (isMovingModeDisabled.Value)
        {
            var position = transform.position;
            var targetPosition = target.transform.position;
            var targetPos = new Vector3(targetPosition.x, targetPosition.y, position.z);
            position = Vector3.MoveTowards(position, targetPos, maxMovingSpeed);
            transform.position = position;
        }
    }

    private void Zoom(float increment)
    {
        var position = transform.position;
        
        var targetPosition = MouseWorldPosition();
        var desiredVector = targetPosition - position;
        var desiredHeight = desiredVector.z * increment + position.z;
        var clampedHeight = Mathf.Clamp(desiredHeight, minHeight, maxHeight);
        var realHeightIncrement = (clampedHeight - position.z)/desiredVector.z;
        
        position += desiredVector * realHeightIncrement;    
        
        transform.position = position;
    }

    public void ResetPosition()
    {
        transform.position = initialPosition;
    }
}
