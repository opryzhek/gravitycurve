using System.Collections;
using ScriptableObjectArchitecture;
using Trajectory;
using UnityEngine;

public class Star: MonoBehaviour, ICollisionProcessor
{
    public BoolVariable isSimulationRunning;
    
    private void Start()
    {
        isSimulationRunning.AddListener(() =>
        {
            if (!isSimulationRunning.Value)
            {
                transform.localScale = Vector3.one;
            }
            StopAllCoroutines();
        });
    }

    public void OnCollision(CollisionInfo collisionInfo)
    {
        if (!ShouldCollide(collisionInfo)) return;
        StartCoroutine(AnimateStar());
    }

    private bool ShouldCollide(CollisionInfo collisionInfo)
    {
        // ReSharper disable once Unity.ExplicitTagComparison
        return collisionInfo.otherBody.tag == "Player";
    }

    public CollisionProcessingResult ProcessCollision(CollisionInfo collisionInfo)
    {
        if (!ShouldCollide(collisionInfo))
            return new CollisionProcessingResult(collisionInfo.myBody);
        return new CollisionProcessingResult(new Body(collisionInfo.myBody.gameObject,
            collisionInfo.myBody.position, collisionInfo.myBody.indexInWorld));
    }

    IEnumerator AnimateStar()
    {
        while (transform.localScale.x > 0)
        {
            var transform1 = transform;
            transform1.localScale -= Vector3.one * 0.02f;
            transform.rotation = Quaternion.RotateTowards(transform1.rotation, Quaternion.identity, 1f);
            yield return 0;
        }
        transform.localScale = Vector3.zero;
    }
}