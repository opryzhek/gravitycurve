﻿using UnityEngine;

public class DestroyTimer: MonoBehaviour
{
    [SerializeField]
    public float timeToDestroy;

    void Start()
    {
        Destroy(gameObject, timeToDestroy);
    }
}
