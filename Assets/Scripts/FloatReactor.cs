﻿using System.Collections;
using System.Collections.Generic;
using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FloatReactor : MonoBehaviour
{
    [SerializeField] private FloatVariable floatVariable;
    [SerializeField] private FloatUnityEvent onChange;
    void Start()
    {
        floatVariable.AddListener(() =>
        {
            onChange.Invoke(floatVariable.Value);
        });   
    }
}
