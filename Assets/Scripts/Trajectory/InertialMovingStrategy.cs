﻿using System;
using UnityEngine;

namespace Trajectory
{
    public class InertialMovingStrategy : MonoBehaviour, IMovingStrategy
    {
        public Vector3 startVelocity;
        private Vector3[] velocities = new Vector3[100];
        

        private IVelocityChanger[] velocityChangers;
        private float deltaTime;

        private void Awake()
        {
            velocityChangers = GetComponents<IVelocityChanger>();
            deltaTime = Time.fixedDeltaTime;
        }

        public Body MakeStep(Body body, TrajectoryStep step)
        {
            var velocity = startVelocity;
            if (step.step > 0)
            {
                velocity = velocities[step.step - 1];
            }
            foreach (var velocityChanger in velocityChangers)
            {
                velocity += velocityChanger.makeStep(body, step);
            }
            
            if (step.step >= velocities.Length)
            {
                EnlargeTimeline();
            }

            velocities[step.step] = velocity;
            return new Body(body, body.position + velocity * deltaTime);
        }
        
        private void EnlargeTimeline() {
            var larger = new Vector3[velocities.Length * 2];
            Array.Copy(velocities, larger, velocities.Length);
            velocities = larger;
        }
    }
}