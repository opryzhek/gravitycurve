namespace Trajectory
{
    public class TrajectoryStep
    {
        public readonly Body[][] timeline;
        public readonly int step;

        public Body[] World => timeline[step];
        public Body[] PrevWorld => timeline[step > 0 ? step-1 : 0];
        public TrajectoryStep(Body[][] timeline, int step)
        {
            this.timeline = timeline;
            this.step = step;
        }
    }
}