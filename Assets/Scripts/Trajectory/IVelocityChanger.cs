using UnityEngine;

namespace Trajectory
{
    public interface IVelocityChanger
    {
        Vector3 makeStep(Body body, TrajectoryStep step);
    }
}