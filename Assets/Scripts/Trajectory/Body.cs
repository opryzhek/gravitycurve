﻿using System;
using System.Collections.Generic;
using System.Resources;
using UnityEngine;

namespace Trajectory
{
    [System.Serializable]
    public struct Body
    {
        public readonly float mass;
        public readonly Vector3 position;
        public readonly float size;
        public IMovingStrategy movingStrategy;
        public readonly ICollisionProcessor collisionProcessor;
        public readonly bool isTrigger;
        public readonly GameObject gameObject;
        public readonly int indexInWorld;
        public readonly String tag;

        public Body(GameObject gameObject, Vector3 position, int indexInWorld) : this(0, position, 
            0, StaticMovingStrategy.instance,
            IgnoreCollisionProcessor.instance, true, gameObject, "", indexInWorld) {}
        public Body(float mass, Vector3 position, float size, IMovingStrategy movingStrategy,
            ICollisionProcessor collisionProcessor,
            bool isTrigger, GameObject gameObject, String tag, int indexInWorld)
        {
            this.mass = mass;
            this.position = position;
            this.size = size;
            this.movingStrategy = movingStrategy;
            this.isTrigger = isTrigger;
            this.gameObject = gameObject;
            this.collisionProcessor = collisionProcessor;
            this.tag = tag;
            this.indexInWorld = indexInWorld;
        }

        public Body(Body copy, Vector3 position)
        {
            this.position = position;
            indexInWorld = copy.indexInWorld;
            mass = copy.mass;
            movingStrategy = copy.movingStrategy;
            size = copy.size;
            isTrigger = copy.isTrigger;
            gameObject = copy.gameObject;
            collisionProcessor = copy.collisionProcessor;
            tag = copy.tag;
        }

        public Body MakeStep(TrajectoryStep step)
        {
            return movingStrategy.MakeStep(this, step);
        }

        public bool Equals(Body other)
        {
            return mass.Equals(other.mass) && position.Equals(other.position);
        }

        public override bool Equals(object obj)
        {
            return obj is Body other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (mass.GetHashCode() * 397) ^ position.GetHashCode();
            }
        }

        public static bool operator ==(Body left, Body right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Body left, Body right)
        {
            return !left.Equals(right);
        }

        public override string ToString()
        {
            return $"{nameof(mass)}: {mass}, {nameof(position)}: {position}, {nameof(size)}: {size}, {nameof(movingStrategy)}: {movingStrategy}, {nameof(collisionProcessor)}: {collisionProcessor}, {nameof(isTrigger)}: {isTrigger}, {nameof(gameObject)}: {gameObject}";
        }
    }
}