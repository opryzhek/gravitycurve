using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Game;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Trajectory
{
    public class WorldController: MonoBehaviour
    {
        [SerializeField] private BodyCollection initialState;
        private Body[] firstWorld;
        private Body[][] timeline;
        private List<CollisionInfo>[] collisions;
        public IntVariable trajectoryStep;
        public IntReference trajectoryPrecalcSize;
        private int timelineSize;
        private int lastTimelineStep;
        public bool isTimelineStopped;
        private Selectable timelineStopCause;
        private ConcurrentBag<Action> executionQueue = new ConcurrentBag<Action>();

        private void Awake()
        {
            collisions = new List<CollisionInfo>[trajectoryPrecalcSize + 10];
            timeline = new Body[trajectoryPrecalcSize + 10][];
        }

        private void Start()
        {
            firstWorld = initialState.ToArray();
            ResetTimeline(0);
        }

        public Vector3[] GetTrajectoryForBodyWithIndex(int index, int fromStep = 0, int length = -1)
        {
            if (fromStep > timelineSize)
            {
                return new Vector3[0];
            }
            if (length < 0)
            {
                length = timelineSize;
            }
            var resultSize = Mathf.Min(
                (isTimelineStopped? lastTimelineStep+1 : timelineSize)-fromStep,
                length) ;
            var result = new Vector3[resultSize];
            for (int i = fromStep; i < resultSize + fromStep; i++)
            {
                result[i-fromStep] = timeline[i][index].position;
            }
            return result;
        }
        
        public void ResetTimeline(int step)
        {
            if (step > timelineSize)
            {
                return;
            }
            
            for (int i = step; i < collisions.Length; i++)
            {
                collisions[i]?.Clear();
            }
            executionQueue.Add(() =>
            {
                timelineStopCause?.SetSelected(false);
                timelineStopCause = null;
            });
            timelineSize = step;
            isTimelineStopped = false;
            lastTimelineStep = -1;
        }

        public Vector3 GetPosition(int bodyIndex, int step)
        {
            EnsureStep(step);
            return timeline[step][bodyIndex].position;
        }

        public IEnumerable<CollisionInfo> GetCollisions(int step, int bodyIndex)
        {
            EnsureStep(step);
            if (collisions[step] != null)
            {
                return collisions[step].FindAll(it => it.myBodyIndex == bodyIndex);
            }

            return Enumerable.Empty<CollisionInfo>();
        }

        public void EnsureStep(int step)
        {
            while (timelineSize <= step)
            {
                MakeStep();
            }
        }

        private void MakeStep()
        {
            if (timelineSize <= 0)
            {
                timeline[0] = firstWorld;
                timelineSize = 1;
                return;
            }

            if (timelineSize >= timeline.Length)
            {
                EnlargeTimeline();
            }
            var prevWorld = timeline[timelineSize - 1];
            var newWorld = new Body[prevWorld.Length];
            for (var i = 0; i < prevWorld.Length; i++)
            {
                newWorld[i] = prevWorld[i].MakeStep(new TrajectoryStep(timeline, timelineSize-1));
            }
            
            timeline[timelineSize] = newWorld;
            
            for (var i = 0; i < newWorld.Length; i++)
            {
                ProcessCollisions(i, newWorld, timelineSize);
            }
            timelineSize++; 
        }
        
        private void EnlargeTimeline() {
            var largerTimeline = new Body[timeline.Length * 2][];
            Array.Copy(timeline, largerTimeline, timelineSize);
            timeline = largerTimeline;
            var largerCollisions = new List<CollisionInfo>[timeline.Length];
            Array.Copy(collisions, largerCollisions, collisions.Length);
            collisions = largerCollisions;
        }
        
        private void ProcessCollisions(int myPosition, Body[] world, int step)
        {
            var myBody = world[myPosition];
            var collisionsForStep = collisions[step] ?? new List<CollisionInfo>();
            for (int i = 0; i < world.Length; i++)
            {
                var body = world[i];
                if (myPosition == i) continue; // ignore self
                if ((body.position - myBody.position).sqrMagnitude >
                      (body.size + myBody.size) * (body.size + myBody.size) / 4) continue; // didn't collide
                var collision = new CollisionInfo(
                    myBody,
                    body,
                    myPosition,
                    i,
                    (myBody.position + body.position) / 2
                );
                collisionsForStep.Add(collision);
                var collisionResult = myBody.collisionProcessor?.ProcessCollision(collision);
                if (collisionResult?.shouldStopTimeline == true)
                {
                    isTimelineStopped = true;
                    lastTimelineStep = step;
                    executionQueue.Add(() =>
                    {
                        timelineStopCause = collision.otherBody.gameObject.GetComponent<Selectable>();
                        timelineStopCause?.SetSelected(true);
                    });
                }
                timeline[step][myPosition] = collisionResult?.body ?? myBody;
            }

            if (collisionsForStep.Count > 0)
            {
                collisions[step] = collisionsForStep;
            }
        }

        private void Update()
        {
            while (executionQueue.TryTake(out var action))
            {
                action.Invoke();
            }
        }
    }
}