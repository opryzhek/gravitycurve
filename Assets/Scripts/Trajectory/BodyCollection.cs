using Trajectory;
using UnityEngine;
namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "BodyCollection.asset",
        menuName = SOArchitecture_Utility.COLLECTION_SUBMENU + "Structs/Body",
        order = SOArchitecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 15)]
    public class BodyCollection : Collection<Body>
    {
    } 
}