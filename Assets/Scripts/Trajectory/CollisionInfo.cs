using UnityEngine;

namespace Trajectory
{
    public struct CollisionInfo
    {
        public Body myBody;
        public Body otherBody;
        public int myBodyIndex;
        public int otherBodyIndex;
        public Vector3 position;

        public CollisionInfo(Body myBody, Body otherBody, int myBodyIndex, int otherBodyIndex, Vector3 position)
        {
            this.myBody = myBody;
            this.otherBody = otherBody;
            this.myBodyIndex = myBodyIndex;
            this.otherBodyIndex = otherBodyIndex;
            this.position = position;
        }

        public override string ToString()
        {
            return $"{nameof(myBody)}: {myBody}, {nameof(otherBody)}: {otherBody}, {nameof(myBodyIndex)}: {myBodyIndex}, {nameof(otherBodyIndex)}: {otherBodyIndex}, {nameof(position)}: {position}";
        }
    }

    public struct CollisionProcessingResult
    {
        public Body body;
        public bool shouldStopTimeline;

        public CollisionProcessingResult(Body body, bool shouldStopTimeline = false)
        {
            this.body = body;
            this.shouldStopTimeline = shouldStopTimeline;
        }
    }

    public interface ICollisionProcessor
    {
        void OnCollision(CollisionInfo collisionInfo);
        CollisionProcessingResult ProcessCollision(CollisionInfo collisionInfo);
    }
    public class IgnoreCollisionProcessor : ICollisionProcessor
    {
        public static IgnoreCollisionProcessor instance = new IgnoreCollisionProcessor();
        private IgnoreCollisionProcessor() {}
        public void OnCollision(CollisionInfo collisionInfo)
        { }

        public CollisionProcessingResult ProcessCollision(CollisionInfo collisionInfo)
        {
            return new CollisionProcessingResult(collisionInfo.myBody);
        }
    }
}