﻿using System;
using ScriptableObjectArchitecture;
using UnityEngine;
using Object = System.Object;

namespace Trajectory
{
    public class GravityBody : MonoBehaviour
    {
        public BoolVariable isSimulationRunning;
        public BoolVariable __isMoving;
        public WorldController worldController ;
        public IntVariable movementStep;
        [SerializeField] public FloatReference mass;
        [SerializeField] public FloatReference size;
        [SerializeField] public BoolReference isTrigger;
        [SerializeField] public Body body;
        [SerializeField] private BodyCollection bodyRegistrar;
        public int IndexInWorld { get; private set; }

        private ICollisionProcessor collisionProcessor;
        private IMovingStrategy movingStrategy;
        private Vector3 startPosition;
        private Quaternion startRotation;
        private void Awake()
        {
            movingStrategy = GetComponent<IMovingStrategy>();
            collisionProcessor = GetComponent<ICollisionProcessor>();

            if (movingStrategy == null)
            {
                movingStrategy = StaticMovingStrategy.instance;
            }
            
            IndexInWorld = bodyRegistrar.Count;
            body = new Body(mass, transform.position, size, movingStrategy, collisionProcessor,
                isTrigger, gameObject, gameObject.tag, IndexInWorld);
            bodyRegistrar.Add(body);
            startPosition = transform.position;
            startRotation = transform.rotation;
            if (__isMoving == null)
            {
                __isMoving = isSimulationRunning;
            }
            isSimulationRunning.AddListener(() =>
            {
                if (!isSimulationRunning.Value)
                {
                    ResetPosition();
                }
            });
            
            movementStep.AddListener(MoveIfNeeded);
        }

        public void MoveIfNeeded()
        {
            foreach (var collision in worldController.GetCollisions(movementStep.Value, IndexInWorld))
            {
                collisionProcessor?.OnCollision(collision);
            }
            if (!__isMoving.Value) return;
            if(Object.Equals(movingStrategy, StaticMovingStrategy.instance)) return;
            transform.position = worldController.GetPosition(IndexInWorld, movementStep.Value);
        }

        private void ResetPosition()
        {
            transform.position = startPosition;
            transform.rotation = startRotation;
        }
    }
}