using System;
using System.Collections.Generic;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Trajectory
{
    public class InputVelocityChanger: MonoBehaviour, IVelocityChanger
    {
        [SerializeField] private IntVariable simulationStep;
        [SerializeField] private FloatReference speed;
        [SerializeField] private Vector3Variable direction;
        [SerializeField] private BoolReference isSimulating;
        [SerializeField] private FloatVariable fuelAmount;
        [SerializeField] private FloatReference maxFuel;
        [SerializeField] private FloatReference fuelPerAccel;
        
        private TrajectoryController trajectoryController;
        private Dictionary<int, Vector3> accelerations = new Dictionary<int, Vector3>();
        private int waitTillApply = 1;
        private int stepsToUpdate = 2;
        private int stepsUntilUpdate = 0;
        private Vector3 accelerationDirection;
        
        private void Start()
        {
            trajectoryController = GetComponent<TrajectoryController>();
            direction.AddListener(() => { accelerationDirection = direction.Value; });
            fuelAmount.Value = 1f;
            isSimulating.AddListener(() => fuelAmount.Value = 1f);
        }

        public Vector3 makeStep(Body body, TrajectoryStep step)
        {
            var result = Vector3.zero;
            if (accelerations.ContainsKey(step.step))
            {
                result += accelerations[step.step];
                accelerations.Remove(step.step);
            }
            return result;
        }

        public void Accelerate(Vector3 direction)
        {
            if (stepsUntilUpdate > 0)
            {
                return;
            }
            for (int i = 0; i < stepsToUpdate; i++)
            {
                accelerations[simulationStep.Value + waitTillApply + i] = direction.normalized * speed;
            }
            trajectoryController.RecalculateTrajectory(simulationStep.Value+waitTillApply);
            stepsUntilUpdate = stepsToUpdate;
            fuelAmount.Value -= fuelPerAccel * direction.magnitude / maxFuel;
        }

        private void Update()
        {
            if (isSimulating.Value && accelerationDirection != Vector3.zero)
            {
                Accelerate(accelerationDirection);
            }
            stepsUntilUpdate--;
        }
    }
}