﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Trajectory
{
    public class TrajectoryController : MonoBehaviour
    {
        [SerializeField] private IntVariable lastTrajectoryStep;
        [SerializeField] private IntReference iterations;
        [SerializeField] private WorldController worldController;
        [SerializeField] private IntVariable trajectoryStep;
        [SerializeField] private BoolVariable targetModeEnabled;
        [SerializeField] private BoolVariable isMoving;
        [SerializeField] private BoolVariable isSimulationRunning;
        [SerializeField] private IntVariable movingStep;
        [SerializeField] private Vector3Reference startDirection;
        [SerializeField] private float launchForceScale;
        [SerializeField] private int framesToRedaw;

        private bool recalcRequested;
        private int recalcRequestedFromStep;

        private TrajectoryBuilder trajectoryBuilder;

        void Start()
        {
            trajectoryBuilder = new TrajectoryBuilder(worldController, lastTrajectoryStep, trajectoryStep);
            trajectoryBuilder.StartCalculation();

            var movingStrategy = GetComponent<InertialMovingStrategy>();
            startDirection.AddListener(() =>
            {
                if (startDirection == Vector3.zero)
                {
                    return;
                }
                if (targetModeEnabled.Value && !isSimulationRunning.Value)
                {
                    movingStrategy.startVelocity = startDirection.Value * launchForceScale;
                    RequestRecalc();
                }
            });
            
            var wasTargetModeEnabled = targetModeEnabled.Value;
            isSimulationRunning.AddListener(() =>
            {
                if (!isSimulationRunning.Value)
                {
                    RecalculateTrajectory();
                }
                movingStep.Value = 0;
                lastTrajectoryStep.Value = iterations;
                isMoving.Value = isSimulationRunning.Value;
                if (isSimulationRunning.Value)
                {
                    wasTargetModeEnabled = targetModeEnabled.Value;
                    targetModeEnabled.Value = true;
                }
                else
                {
                    transform.localScale = Vector3.one;
                    targetModeEnabled.Value = wasTargetModeEnabled;
                }
            });
            lastTrajectoryStep.Value = iterations;
        }

        public void RequestRecalc(int fromStep = 0)
        {
            recalcRequested = true;
            recalcRequestedFromStep = fromStep;
        }

        private int stepsSinceLastRecalc;
        private void LateUpdate()
        {
            if (recalcRequested && stepsSinceLastRecalc > framesToRedaw)
            {
                RecalculateTrajectory(recalcRequestedFromStep);
                recalcRequested = false;
                stepsSinceLastRecalc = -1;
            }

            stepsSinceLastRecalc++;
        }

        private void FixedUpdate()
        {
            if (!isSimulationRunning.Value) return;
            movingStep.Value++;
            lastTrajectoryStep.Value++;
        }

        public void OnDestroy()
        {
            trajectoryBuilder.Dispose();
        }
        
        
        public void RecalculateTrajectory(int fromStep = 0) {
            trajectoryBuilder.RecalculateTrajectory(fromStep);
        }
    }
}