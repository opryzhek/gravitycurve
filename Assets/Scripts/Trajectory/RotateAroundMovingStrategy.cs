using System.Linq;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Trajectory
{
    public class RotateAroundMovingStrategy : MonoBehaviour, IMovingStrategy
    {
        public GravityBody center;
        public FloatReference speed;
        private int centerIndex;
        private Vector3 startRadius = Vector3.zero;

        public Body MakeStep(Body body, TrajectoryStep step)
        {
            if (startRadius == Vector3.zero)
            {
                startRadius = step.World[body.indexInWorld].position -
                              step.World[center.IndexInWorld].position;
            }
            var centerBody = step.World[center.IndexInWorld];
            var vecFromCenter = Quaternion.Euler(0, 0, speed * step.step) * startRadius;
            return new Body(body, centerBody.position + vecFromCenter);
        }
    }
}