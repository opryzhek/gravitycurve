using System;
using System.Linq;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Trajectory
{
    [RequireComponent(typeof(LineRenderer))]
    public class TrajectoryRenderer: MonoBehaviour
    {
        private LineRenderer lineRenderer;
        [SerializeField]
        private WorldController worldController;

        [SerializeField] private IntVariable movementStep;
        [SerializeField] private BoolVariable isSimulationRunning;
        [SerializeField] private BoolVariable isMoving;
        [SerializeField] private IntVariable trajectoryStep;
        [SerializeField] private IntReference minTrajectoryRendered;
        
        private int indexInWorld;
        private bool shouldResetPositions;

        private void Start()
        {
            lineRenderer = GetComponent<LineRenderer>();
            indexInWorld = GetComponentInParent<GravityBody>().IndexInWorld;
            movementStep.AddListener(InvalidateTrajectory);
            trajectoryStep.AddListener(InvalidateTrajectory);
        }


        private void InvalidateTrajectory()
        {
            shouldResetPositions = true;
        }
        
        private Vector3[] oldPositions;
        public void Update()
        {
            if (!shouldResetPositions) return;
            shouldResetPositions = false;
            
            if (isSimulationRunning.Value && !isMoving.Value)
            {
                lineRenderer.positionCount = 0;
                return;
            }
            var newPositions = worldController.GetTrajectoryForBodyWithIndex(indexInWorld, movementStep.Value);
            var positions = newPositions;
            if (newPositions.Length < minTrajectoryRendered.Value &&
                !worldController.isTimelineStopped &&
                oldPositions != null &&
                oldPositions.Length - 1 > newPositions.Length)
            {
                positions = oldPositions.Skip(1).ToArray();
            }
            lineRenderer.positionCount = positions.Length;
            lineRenderer.SetPositions(positions);
            oldPositions = positions;
        }
    }
}