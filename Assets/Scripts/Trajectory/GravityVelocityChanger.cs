﻿using ScriptableObjectArchitecture;
using UnityEngine;

namespace Trajectory
{
    public class GravityVelocityChanger : MonoBehaviour, IVelocityChanger
    {
        [SerializeField] private FloatReference gravityConstant;

        public Vector3 makeStep(Body body, TrajectoryStep step)
        {
            var additionalSpeed = Vector3.zero;
            foreach (var gravityBody in step.World)
            {
                var dif = gravityBody.position - body.position;
                var sqrMagnitude = dif.sqrMagnitude;
                if (sqrMagnitude < Mathf.Epsilon || sqrMagnitude > 10000f)
                {
                    continue;
                }
                
                var force = gravityConstant * body.mass * gravityBody.mass / sqrMagnitude;
                additionalSpeed += dif.normalized * force;
            }

            return additionalSpeed;
        }
    }
}