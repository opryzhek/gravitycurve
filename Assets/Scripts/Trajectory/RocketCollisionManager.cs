﻿using System.Collections;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Trajectory
{
    public class RocketCollisionManager : MonoBehaviour, ICollisionProcessor
    {
        public GameObject explosion;
        public FloatReference shrinkingSpeed;
        public BoolVariable isMoving;
        public BoolVariable isSimulationRunning;
    
        private void Start()
        {
            isSimulationRunning.AddListener(() =>
            {
                StopAllCoroutines();
            });
        }

        public void OnCollision(CollisionInfo collisionInfo)
        { 
            if (collisionInfo.otherBody.isTrigger) return;
            OnCollision(collisionInfo.otherBody);
        }

        public CollisionProcessingResult ProcessCollision(CollisionInfo collisionInfo)
        {
            if (collisionInfo.otherBody.isTrigger) 
                return new CollisionProcessingResult(collisionInfo.myBody);
            return new CollisionProcessingResult(
                new Body(
                    collisionInfo.myBody.gameObject,
                    collisionInfo.myBody.position,
                    collisionInfo.myBody.indexInWorld
                ), true);
        }

        public void OnCollision(Body other)
        {
            if (!isMoving.Value)
            {
                return;
            }
            if (other.gameObject.CompareTag("Portal"))
            {
                StartCoroutine(Teleport(other.gameObject));
                return;
            }

            StartCoroutine(Die());
        }

        private IEnumerator Die()
        {
        
            Instantiate(explosion, transform.position, transform.rotation);
            transform.localScale = Vector3.zero;
            isMoving.Value = false;
            yield return new WaitForSeconds(explosion.GetComponent<DestroyTimer>().timeToDestroy);
            isSimulationRunning.Value = false;
        }

        private IEnumerator Teleport(GameObject portal)
        {
            isMoving.Value = false;
            var startScale = transform.localScale;
            var startDif = portal.transform.position - transform.position;
            var step = 0;
            while (shrinkingSpeed*step < 1f)
            {
                transform.localScale -= startScale * shrinkingSpeed;
                transform.position = portal.transform.position - startDif * (1f - shrinkingSpeed*step);
                step++;
                yield return 0;
            }
            isSimulationRunning.Value = false;
        }
    
    }
}
