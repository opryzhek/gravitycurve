using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ScriptableObjectArchitecture;
using UnityEngine;
using ThreadPriority = System.Threading.ThreadPriority;

namespace Trajectory
{
    public class TrajectoryBuilder : IDisposable
    {
        private readonly WorldController worldController;
        private readonly int iterations;
        private readonly IntVariable step;
        private readonly Semaphore semaphore = new Semaphore(0, 1);
        private bool interruptThread = false;
        private Body[] tempWorld;
        private bool rerun = false;
        private bool isWaiting = false;
        private int rerunFromStep = 0;
        private readonly IntVariable lastTrajectoryStep; 
        private int lastTrajectoryStepCalculated;
        private bool isThreadRunning;

        public TrajectoryBuilder(
            WorldController worldController,
            IntVariable lastTrajectoryStep,
            IntVariable step)
        {
            this.worldController = worldController;
            this.lastTrajectoryStep = lastTrajectoryStep;
            this.step = step;
        }

        public void StartCalculation()
        {
            var thread = new Thread(
                () =>
                {
                    while (true)
                    {
                        
                        isWaiting = true;
                        semaphore.WaitOne(); 
                        if (interruptThread) return;
                        step.Value = rerunFromStep;
                        worldController.ResetTimeline(rerunFromStep);
                        int i;
                        rerun = false;
                        lastTrajectoryStepCalculated = rerunFromStep;
                        for (i = rerunFromStep; i < lastTrajectoryStep.Value; i++)
                        {
                            if (rerun)
                            {
                                rerun = false;
                                if (rerunFromStep < i)
                                {
                                    break;
                                }
                            }
                            worldController.EnsureStep(i+1);
                            lastTrajectoryStepCalculated = i + 1;
                            step.Value++;
                            if (worldController.isTimelineStopped)
                            {
                                break;
                            }
                        }    
                        
                    }
                });

            thread.Priority = ThreadPriority.Normal;
            thread.Start();
            
            lastTrajectoryStep.AddListener(() =>
            {
                if (isWaiting && !worldController.isTimelineStopped && lastTrajectoryStep.Value > lastTrajectoryStepCalculated)
                {
                    rerunFromStep = lastTrajectoryStepCalculated;
                    isWaiting = false;
                    semaphore.Release();
                }
            });
        }
        

        public void RecalculateTrajectory(int fromStep)
        {
            rerunFromStep = fromStep;
            rerun = true;
            if (isWaiting)
            {
                isWaiting = false;
                semaphore.Release();
            }
        }
        
        public void Dispose()
        {
            interruptThread = true;
        }
        
    }
}