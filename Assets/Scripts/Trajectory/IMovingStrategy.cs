using System.Collections.Generic;

namespace Trajectory
{
    public interface IMovingStrategy
    {
        Body MakeStep(Body body, TrajectoryStep step);
    }
    

    public class StaticMovingStrategy: IMovingStrategy
    {
        private StaticMovingStrategy() {}
        public static StaticMovingStrategy instance = new StaticMovingStrategy();
        public Body MakeStep(Body body, TrajectoryStep step)
        {
            return body;
        }
    }
}