﻿using ScriptableObjectArchitecture;
using UnityEngine;

namespace Game
{
    public class RocketRotator : MonoBehaviour
    {
        [SerializeField] private BoolReference shouldRotate;
        [SerializeField] private FloatReference rotationSpeed;
        [SerializeField] private Vector3Reference joystickDirection;
        private Vector3 prevPosition;
        private bool rotatedLastFrame = false;
        private Vector3 rotateTowardsVector;
        private bool hasRotateToDirection;


        void Update()
        {
            ProcessRotateToDirection();
            var hasInput = joystickDirection.Value != Vector3.zero;
            var targetRotationVector = hasInput
                ? joystickDirection.Value 
                : rotateTowardsVector;
            var targetRotation = Quaternion.LookRotation(Vector3.forward, targetRotationVector);
            if (shouldRotate && (hasInput || hasRotateToDirection))
            {
                transform.rotation =
                    Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed);
            }
            
        }

        private void ProcessRotateToDirection()
        {
            hasRotateToDirection = false;
            if (!shouldRotate)
            {
                rotatedLastFrame = false;
                return;
            }
            if (!rotatedLastFrame)
            {
                prevPosition = transform.position;
                rotatedLastFrame = true;
                return;
            }
            var dif = transform.position - prevPosition;
            if (dif.sqrMagnitude > Mathf.Epsilon)
            {
                hasRotateToDirection = true;
                prevPosition = transform.position;
                rotateTowardsVector = dif;
            }
        }
    }
}
