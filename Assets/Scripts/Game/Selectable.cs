namespace Game
{
    public interface Selectable
    {
        void SetSelected(bool isSelected);
    }
}