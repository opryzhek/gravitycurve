using System;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(Renderer))]
    public class ColorChanger : MonoBehaviour, Selectable
    {
        private const string COLOR_NAME = "_Color";
        private static readonly int colorPropertyId = Shader.PropertyToID(COLOR_NAME);

        [SerializeField] private ColorReference usualColor;
        [SerializeField] private ColorReference selectedColor;
        [SerializeField] private int framesTillUpdate = 3;

        private new Renderer renderer;
        private MaterialPropertyBlock propertyBlock;
        private bool wasSelected = false;
        private bool willBeSelected = false;
        private int framesLeft = 0;
        private void Start()
        {
            propertyBlock = new MaterialPropertyBlock();
            renderer = GetComponent<Renderer>();
            SetColor(usualColor);
        }

        public void SetSelected(bool isSelected)
        {
            willBeSelected = isSelected;
            if (!isSelected)
            {
                framesLeft = framesTillUpdate;
            }
        }

        private void Update()
        {
            framesLeft--;
            if (framesLeft <= 0)
            {
                framesLeft = framesTillUpdate;
                SetSelectedInternal(willBeSelected);
            }
        }
        
        private  void SetSelectedInternal(bool isSelected)
        {
            if (wasSelected == isSelected)
            {
                return;
            }
            SetColor(isSelected ? selectedColor : usualColor);
            wasSelected = isSelected;
        }

        private void SetColor(Color color)
        {
            propertyBlock.SetColor(colorPropertyId, color);
            renderer.SetPropertyBlock(propertyBlock);
        }
    }
}