using System;
using ScriptableObjectArchitecture;
using UnityEngine;

namespace Game
{
    public class ParticleColorChanger : MonoBehaviour, Selectable
    {
        [SerializeField] private ColorReference selectedColor;
        [SerializeField] private int framesTillUpdate = 3;
        [SerializeField] new ParticleSystem particleSystem;

        private Color usualColor;
        private bool wasSelected = false;
        private bool willBeSelected = false;
        private int framesLeft = 0;
        private void Start()
        {
            var main = particleSystem.main;
            usualColor = main.startColor.color;
        }

        public void SetSelected(bool isSelected)
        {
            willBeSelected = isSelected;
            if (!isSelected)
            {
                framesLeft = framesTillUpdate;
            }
        }

        private void Update()
        {
            framesLeft--;
            if (framesLeft <= 0)
            {
                framesLeft = framesTillUpdate;
                SetSelectedInternal(willBeSelected);
            }
        }
        
        private  void SetSelectedInternal(bool isSelected)
        {
            if (wasSelected == isSelected)
            {
                return;
            }
            SetColor(isSelected ? selectedColor : usualColor);
            wasSelected = isSelected;
        }

        private void SetColor(Color color)
        {
            var main = particleSystem.main;
            main.startColor = color;
        }
    }
}