﻿using ScriptableObjectArchitecture;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(ParticleSystem))]
    public class EngineController : MonoBehaviour
    {
        [SerializeField] private Vector3Variable direction;
        [SerializeField] private BoolVariable isSimulationRunning;
        private new ParticleSystem particleSystem;

        private float startStartSpeedMultiplier;
        private float startEmissionRateOverTimeMultiplier;
        private void Start()
        {
            particleSystem = GetComponent<ParticleSystem>();
            startStartSpeedMultiplier = particleSystem.main.startSpeedMultiplier;
            startEmissionRateOverTimeMultiplier = particleSystem.emission.rateOverTimeMultiplier;
            direction.AddListener(OnDirectionChange);
            isSimulationRunning.AddListener(OnDirectionChange);
            OnDirectionChange();
        }

        private void OnDirectionChange()
        {
            var magnitude = direction.Value.magnitude;
            if (!isSimulationRunning.Value && magnitude < Mathf.Epsilon)
            {
                return;
            }
            var main = particleSystem.main;
            main.startSpeedMultiplier = startStartSpeedMultiplier * magnitude;
//            var emission = particleSystem.emission;
//            emission.rateOverTimeMultiplier = startEmissionRateOverTimeMultiplier * magnitude;
        }
    }
}
